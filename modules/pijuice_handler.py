#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import threading
import time
from pijuice import PiJuice
from datetime import datetime


class Pijuice(threading.Thread):

    def __init__(self, log_obj, alarm):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.alarm = alarm
        self.pijuice = PiJuice(1, 0x14)

        self.status = None
        self.charge = None
        self.fault = None
        self.temp = None
        self.vbat = None
        self.ibat = None
        self.vio = None
        self.iio = None

        self.present_alarm_sent = False

        self.log_obj.info("Pijuice < Initiated >")

    def check_value(self, val):
        val = val['data'] if val['error'] == 'NO_ERROR' else val['error']
        return val

    def get_data(self):
        self.charge = self.check_value(self.pijuice.status.GetChargeLevel())
        self.fault = self.check_value(self.pijuice.status.GetFaultStatus())
        self.temp = self.check_value(self.pijuice.status.GetBatteryTemperature())
        self.vbat = self.check_value(self.pijuice.status.GetBatteryVoltage())
        self.ibat = self.check_value(self.pijuice.status.GetBatteryCurrent())
        self.vio = self.check_value(self.pijuice.status.GetIoVoltage())
        self.iio = self.check_value(self.pijuice.status.GetIoCurrent())
        self.status = self.check_value(self.pijuice.status.GetStatus())

    def check_alaram(self):
        if "COMMUNICATION_ERROR" in self.temp:
            return True
        if self.temp > 60:
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S")
            self.log_obj.info("PIJUICE < High Battery tempereture > < {} >".format(self.temp))
            self.alarm.add_alarm("{} {} {}".format("battemp", self.temp, current_time))
        if self.status['powerInput'] == 'NOT_PRESENT' and not self.present_alarm_sent:
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S")
            self.log_obj.info("PIJUICE < Power cord disconnected >")
            self.alarm.add_alarm("{} {}".format("bnp", current_time))
            self.present_alarm_sent = True
        if self.status['powerInput'] == 'PRESENT' and self.present_alarm_sent:
            self.present_alarm_sent = False

    def run(self):
        while True:
            time.sleep(1)
            self.get_data()
            self.check_alaram()
