#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
if sys.platform == "linux":
    import RPi.GPIO as GPIO


class GpioHandler:

    def __init__(self, log_obj, config):
        self.log_obj = log_obj
        self.led_a_pin = 5
        self.led_b_pin = 6
        self.led_c_pin = 12
        self.led_d_pin = 13
        self.gpio_beeper_pin = 17

        if sys.platform == "linux":
            self.setup_gpio()

    def setup_gpio(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.led_a_pin, GPIO.OUT)
        GPIO.setup(self.led_b_pin, GPIO.OUT)
        GPIO.setup(self.led_c_pin, GPIO.OUT)
        GPIO.setup(self.led_d_pin, GPIO.OUT)

        GPIO.setup(self.gpio_beeper_pin, GPIO.OUT)

    def led_a(self, status):
        if sys.platform == "linux":
            if status:
                GPIO.output(self.led_a_pin, GPIO.HIGH)
            else:
                GPIO.output(self.led_a_pin, GPIO.LOW)
        else:
            print("LED_A {}".format(status))

    def led_b(self, status):
        if sys.platform == "linux":
            if status:
                GPIO.output(self.led_b_pin, GPIO.HIGH)
            else:
                GPIO.output(self.led_b_pin, GPIO.LOW)
        else:
            print("LED_B {}".format(status))

    def led_c(self, status):
        if sys.platform == "linux":
            if status:
                GPIO.output(self.led_c_pin, GPIO.HIGH)
            else:
                GPIO.output(self.led_c_pin, GPIO.LOW)
        else:
            print("LED_C {}".format(status))

    def led_d(self, status):
        if sys.platform == "linux":
            if status:
                GPIO.output(self.led_d_pin, GPIO.HIGH)
            else:
                GPIO.output(self.led_d_pin, GPIO.LOW)
        else:
            print("LED_D {}".format(status))

    def beeper(self, status):
        if sys.platform == "linux":
            if status:
                GPIO.output(self.gpio_beeper_pin, GPIO.HIGH)
            else:
                GPIO.output(self.gpio_beeper_pin, GPIO.LOW)
        else:
            print("BEEPER {}".format(status))


