#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import threading
import time
import sys


if sys.platform == "linux":
    import RPi.GPIO as GPIO


class Mqtt(threading.Thread):

    def __init__(self, log_obj, config, gpio, alarm):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.gpio = gpio
        self.alarm = alarm

        self.connected = False
        self.broker_address = config["mqtt"]["broker_address"]  # Broker address
        self.mqtt_port = int(config["mqtt"]["mqtt_port"])  # Broker port
        self.user = config["mqtt"]["user"]  # Connection username
        self.password = config["mqtt"]["pass"]  # Connection password

    def mqtt_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.log_obj.info("MQTT < Connected to broker >")
            print("Connected to broker")
            self.connected = True
            #self.gpio.led_b("on")
        else:
            self.log_obj.critical("MQTT < Connection failed >")
            raise ValueError("MQTT < Connection failed >")

    def mqtt_message(self, client, userdata, message):

        data = message.payload.decode("utf-8")
        data = ''.join(c for c in data if c not in '(){}<>"')
        data = data.split(",")

        if len(data) > 5:
            rx_id = data[4].replace("Data:", "")
            rx_time = data[0].replace("Time:", '')
            self.alarm.add_alarm("{} {}".format(rx_id, rx_time))
            self.log_obj.info("MQTT < Rx message > < {} {} >".format(rx_time, rx_id))
            print("{} {}".format(rx_time, rx_id))

    def run(self):
        client = mqtt.Client("Python")  # create new instance
        client.username_pw_set(self.user, password=self.password)  # set username and password
        client.on_connect = self.mqtt_connect  # attach function to callback
        client.on_message = self.mqtt_message  # attach function to callback

        client.connect(self.broker_address, port=self.mqtt_port)  # connect to broker

        client.loop_start()  # start the loop

        while not self.connected:  # Wait for connection
            time.sleep(1)

        print("MQTT < Online >")
        self.log_obj.info("MQTT < Online >")

        client.subscribe("dev/test/RESULT")

        try:
            while True:
                time.sleep(1)

        except KeyboardInterrupt:
            print("exiting")
            client.disconnect()
            client.loop_stop()
