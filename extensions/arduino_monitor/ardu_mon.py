# -*- coding: utf-8 -*-

import time
import datetime
import sys
from influxdb import InfluxDBClient
from pijuice import PiJuice
import subprocess

host = "localhost"
port = 8086
user = "acp"
password = "draco"


client = InfluxDBClient(host, port, user, password, "test")
pijuice = PiJuice(1, 0x14)


def get_cpu_temp():
    """Get the core temperature.
    Run a shell script to get the core temp and parse the output.
    Raises:
        RuntimeError: if response cannot be parsed.
    Returns:
        float: The core temperature in degrees Celsius.
    """
    output = subprocess.run(['vcgencmd', 'measure_temp'], capture_output=True)
    temp_str = output.stdout.decode()
    print(temp_str)
    try:
        return float(temp_str.split('=')[1].split('\'')[0])
    except (IndexError, ValueError):
        raise RuntimeError('Could not parse temperature output.')


def check_value(val):
    val = val['data'] if val['error'] == 'NO_ERROR' else val['error']
    return val


def get_data_points():
    timestamp = datetime.datetime.utcnow().isoformat()
    charge = check_value(pijuice.status.GetChargeLevel())
    temp = check_value(pijuice.status.GetBatteryTemperature())
    status = check_value(pijuice.status.GetStatus())
    cpu_temp = get_cpu_temp()

    dp = [
        {
            "measurement": "pi_1",
            "tags": {"runNum": 1,
                     },
            "time": timestamp,
            "fields": {
                "temperaturevalue": temp,
                "charhe_Value": charge,
                "powerInputstatus": status["powerInput"],
                "batterystatus": status["battery"],
                "cuptmpvalue": cpu_temp
            }
        }
    ]
    return dp


while True:
    datapoints = get_data_points()
    bResult = client.write_points(datapoints)
    print("Write points {0} Bresult:{1}".format(datapoints, bResult))
    time.sleep(60)
