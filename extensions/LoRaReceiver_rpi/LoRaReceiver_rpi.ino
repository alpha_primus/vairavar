#include <SPI.h>
#include <LoRa.h>

#include "DHT.h"

#define DHTTYPE DHT11 
#define DHTPIN 7

const int csPin = 6;          // LoRa radio chip select
const int resetPin = 5;       // LoRa radio reset
const int irqPin = 3;         // change for your board; must be a hardware interrupt pin
const byte LDO_EN = 10;

float h = 0;
float t = 0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(LDO_EN, OUTPUT);
  digitalWrite (LDO_EN, HIGH);

  
  Serial.begin(9600);
  while (!Serial);

  dht.begin();


  Serial.println("LoRa Receiver");
  
  LoRa.setPins(csPin, resetPin, irqPin);
  
  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");
    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }
    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());


    h = dht.readHumidity();
    t = dht.readTemperature();

    Serial.print("Received packet '");
    Serial.print("#0");
    Serial.print(" ");
    Serial.print(t);
    Serial.print(" ");
    Serial.print("0.00");
    Serial.print(" ");
    Serial.print(0.00);
    Serial.print(" ");
    Serial.print(h);
    Serial.print("' with RSSI ");
    Serial.println("-00");

    
  }
}
