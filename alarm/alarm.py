#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import threading
import queue
import sys
import time
import ssl
import smtplib
import requests

if sys.platform == "linux":
    import RPi.GPIO as GPIO


class Alarm(threading.Thread):
    def __init__(self, log_obj, config, gpio):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.config = config
        self.gpio = gpio
        self.alarm_on = False
        self.alarm = queue.Queue(maxsize=1024)
        self.active = None

        self.smtp_port = int(config["smtp"]["port"])
        self.smtp_server = config["smtp"]["server"]
        self.sender_email = config["smtp"]["sender_email"]
        self.receiver_email = config["smtp"]["receiver_email"]
        self.email_password = config["smtp"]["pass"]

        self.alarm_swt_on = config["alarm_ids"]["alarm_swt_on"]
        self.alarm_swt_off = config["alarm_ids"]["alarm_swt_off"]
        self.alarm_swt_on_1 = config["alarm_ids"]["alarm_swt_on_1"]
        self.alarm_swt_off_2 = config["alarm_ids"]["alarm_swt_off_1"]

        self.alarm_swt_door_1 = config["alarm_ids"]["door_1"]

        self.alarm_pir_1 = config["alarm_ids"]["pir_1"]
        self.alarm_pir_2 = config["alarm_ids"]["pir_2"]
        self.alarm_pir_3 = config["alarm_ids"]["pir_3"]
        self.alarm_pir_4 = config["alarm_ids"]["pir_4"]
        self.alarm_pir_5 = config["alarm_ids"]["pir_5"]

        self.pijuice_btemp = config["pijuice_ids"]["bat_temp"]
        self.pijuice_bnp = config["pijuice_ids"]["bat_presnt"]

        self.log_obj.debug("Initialized successfully".format())

    def activate_alarm(self):
        self.alarm_on = True
        try:
            requests.get("http://192.168.1.29/on")
            time.sleep(1)
            requests.get("http://192.168.1.30/on")
        except requests.exceptions.RequestException as e:
            # catastrophic error. bail.
            self.log_obj.warning(e)

    def diactivate_alarm(self):
        self.alarm_on = False
        try:
            requests.get("http://192.168.1.29/off")
            time.sleep(1)
            requests.get("http://192.168.1.30/off")
        except requests.exceptions.RequestException as e:
            # catastrophic error. bail.
            self.log_obj.warning(e)

    def add_alarm(self, alarm):
        if self.alarm.full():
            self.log_obj.warning("Alam Queue is full")
            return False
        self.alarm.put(alarm)

    def run(self):
        self.active = True
        while self.active:
            if not self.alarm.empty():
                alarm = self.alarm.get(block=False)
                self.process_alarm(alarm)
        self.active = False
        self.log_obj.debug("Stopping Task Handler")

    def send_email(self, info):
        return True
        message = "Subject: Alaram! " \
                  "{}".format(info)
        context = ssl.create_default_context()
        with smtplib.SMTP(self.smtp_server, self.smtp_port) as server:
            server.ehlo()  # Can be omitted
            server.starttls(context=context)
            server.ehlo()  # Can be omitted
            server.login(self.sender_email, self.email_password)
            server.sendmail(self.sender_email, self.receiver_email, message)

            self.log_obj.info("EMAIL < Send alarm mail> < {} >".format(message))

    def sound_alarm(self):
        gs = False
        while self.alarm_on:
            gs ^= True
            self.gpio.beeper(gs)
            try:
                time.sleep(0.1)
                requests.get("http://192.168.1.29/on")
                time.sleep(0.1)
                requests.get("http://192.168.1.30/on")
                time.sleep(1)
            except requests.exceptions.RequestException as e:
                # catastrophic error. bail.
                self.log_obj.warning(e)

    def process_alarm(self, alarm):

        print("Got Alarm {}".format(alarm))
        data = alarm.split(" ")
        alarm_id = data[0]
        alarm_time = data[1]
        alram_triggered = False

        if self.alarm_swt_on in alarm_id or self.alarm_swt_on_1 in alarm_id:
            if not self.alarm_on:
                self.gpio.beeper(True)
                self.gpio.led_c(True)
                time.sleep(0.1)
                self.gpio.beeper(False)
                self.gpio.led_c(False)
                time.sleep(0.1)
                self.gpio.beeper(True)
                self.gpio.led_c(True)
                time.sleep(0.1)
                self.gpio.beeper(False)
                self.gpio.led_c(False)
                time.sleep(0.1)
                self.gpio.led_c(True)

                self.activate_alarm()

        elif self.alarm_swt_off in alarm_id or self.alarm_swt_off_2 in alarm_id:
            if self.alarm_on:
                self.gpio.beeper(True)
                time.sleep(0.2)
                self.gpio.beeper(False)
                self.gpio.led_c(False)
                self.diactivate_alarm()

        elif self.alarm_swt_door_1 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ door ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.alarm_pir_1 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ ude ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.alarm_pir_2 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ stue ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.alarm_pir_3 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ bryggers ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.alarm_pir_4 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ koekken ved komfur ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.alarm_pir_5 in alarm_id:
            if self.alarm_on:
                info = "{} Alaram from sensor [ carport ] {}".format(alarm_time, alarm_id)
                alram_triggered = True

        elif self.pijuice_btemp in alarm_id:
            info = "{} Alaram from pijiuce {}".format(alarm_time, alarm_id)
            alram_triggered = True

        elif self.pijuice_bnp in alarm_id:
            info = "{} Alaram from pijiuce {}".format(alarm_time, alarm_id)
            #self.send_email(info)

        if alram_triggered:
            #self.send_email(info)
            t1 = threading.Thread(target=self.sound_alarm, args=[])
            t1.start()
