#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import logging.handlers
from datetime import datetime
import sys


def initiate_logging():
    # log_obj.info("yes   hfghghg ghgfh".format())
    # log_obj.critical("CRIC".format())
    # log_obj.error("ERR".format())
    # log_obj.warning("WARN".format())
    # log_obj.debug("debug".format())

    try:

        log_file_name = "logs/homeControl"
        formatter = logging.Formatter(fmt='%(asctime)s %(module)s,line: %(lineno)d %(levelname)8s | %(message)s',
                                      datefmt='%Y/%m/%d %H:%M:%S')  # %I:%M:%S %p AM|PM format
                            
        logging_level = logging.DEBUG
    
        # set TimedRotatingFileHandler for root
        # use very short interval for this example, typical 'when' would be 'midnight' and no explicit interval
        handler = logging.handlers.TimedRotatingFileHandler(log_file_name, when="midnight", interval=1)
        handler.setFormatter(formatter)
        logger = logging.getLogger() # or pass string to give it a name
        logger.addHandler(handler)
        logger.setLevel(logging_level)
        # generate lots of example messages
        return logger
    except Exception as ex:
        # handle unexpected script errors
        logging.exception("Unhandled error\n{}".format(ex))
        raise
    finally:
        # perform an orderly shutdown by flushing and closing all handlers; called at application exit and no further use of the logging system should be made after this call.
        logging.shutdown()


