/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-esp8266-web-server-outputs-momentary-switch/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>

// REPLACE WITH YOUR NETWORK CREDENTIALS
const char* ssid = "Horus";
const char* password = "@qqPQ7BB";

const int output = 2;
const int beeper = D4;

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

bool alarm_activated = false;
bool alarm_deactivated = false;

AsyncWebServer server(80);

void two_beep(void)
{
  digitalWrite(output, HIGH);
  digitalWrite(beeper, HIGH);
  Serial.println("HIGH");
  delay(100);
  digitalWrite(output, LOW);
  digitalWrite(beeper, LOW);
  Serial.println("LOW");
  delay(100);
  digitalWrite(output, HIGH);
  digitalWrite(beeper, HIGH);
  Serial.println("HIGH");
  delay(100);
  digitalWrite(output, LOW);
  digitalWrite(beeper, LOW);
  Serial.println("LOW!");
}

void one_beep(void)
{
  digitalWrite(output, HIGH);
  digitalWrite(beeper, HIGH);
  Serial.println("HIGH");
  delay(500);
  digitalWrite(output, LOW);
  digitalWrite(beeper, LOW);
  Serial.println("LOW");
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Failed!");
    return;
  }
  Serial.println();
  Serial.print("ESP IP Address: http://");
  Serial.println(WiFi.localIP());
  
  pinMode(output, OUTPUT);
  pinMode(beeper, OUTPUT);
  digitalWrite(output, LOW);
  digitalWrite(beeper, LOW);
  
  // Send web page to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    
    request->send_P(200, "text/plain", "ok");
  });

  // Receive an HTTP GET request
  server.on("/on", HTTP_GET, [] (AsyncWebServerRequest *request) {
    alarm_activated = true;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET request
  server.on("/off", HTTP_GET, [] (AsyncWebServerRequest *request) {
    alarm_deactivated = true;
    request->send(200, "text/plain", "ok");
  });
  
  server.onNotFound(notFound);
  server.begin();
}

void loop() {

  if(alarm_activated)
  {
     two_beep();
    alarm_activated = false;
  }
  else if(alarm_deactivated)
  {
    one_beep();
    alarm_deactivated = false;
  }
 
}
