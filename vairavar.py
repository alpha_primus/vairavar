#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import threading
import configparser
import sys

from logs import logger
from modules import mqtt_handler
from modules import gpio_handler

if sys.platform == "linux":
    from modules import pijuice_handler

from alarm.alarm import Alarm


class Vairavar(threading.Thread):

    def __init__(self, log_obj, config):
        threading.Thread.__init__(self)
        self.log_obj = log_obj
        self.config = config
        self.gpio = None
        self.mqtt = None
        self.alarm_handler = None
        self.pijuice = None

    def run(self):

        self.gpio = gpio_handler.GpioHandler(self.log_obj, self.config)
        
        self.alarm_handler = Alarm(self.log_obj, self.config, self.gpio)
        self.alarm_handler.start()

        self.mqtt = mqtt_handler.Mqtt(self.log_obj, self.config, self.gpio, self.alarm_handler)
        self.mqtt.start()

        if sys.platform == "linux":
            self.pijuice = pijuice_handler.Pijuice(self.log_obj, self.alarm_handler)
            self.pijuice.start()

        self.gpio.led_a(False)
        self.gpio.led_b(False)
        try:
            while True:
                time.sleep(1)

        except KeyboardInterrupt:
            print("exiting")


def main():
    config = configparser.ConfigParser()
    if sys.platform == "linux":
        config.read('configs.txt')
    else:
        config.read('configs.txt')
    log_obj = logger.initiate_logging()

    


    vairavar = Vairavar(log_obj, config)
    vairavar.start()


if __name__ == '__main__':
    main()
